package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"time"
)

type TestEvent struct {
	Time    time.Time // encodes as an RFC3339-format string
	Action  string
	Package string
	Test    string
	Elapsed float64 // seconds
	Output  string
	seq     int
}

type pt struct {
	Package string
	Test    string
}

func (t pt) String() string {
	return t.Package + "." + t.Test
}

func main() {
	dec := json.NewDecoder(os.Stdin)
	var err error
	var ev TestEvent
	m := make(map[pt][]TestEvent)
	err = dec.Decode(&ev)
	i := 0
	for err == nil {
		ev.seq = i
		switch ev.Action {
		case "output":
			m[pt{ev.Package, ev.Test}] = append(m[pt{ev.Package, ev.Test}], ev)
		case "skip", "pass":
			delete(m, pt{ev.Package, ev.Test})
		}

		fmt.Print(".")
		i++
		if i%60 == 0 {
			fmt.Println()
		}

		err = dec.Decode(&ev)
	}
	fmt.Println()
	var es []TestEvent
	for _, t := range m {
		for _, e := range t {
			es = append(es, e)
		}
	}
	sort.Slice(es, func(i, j int) bool {
		return es[i].seq < es[j].seq
	})
	for _, e := range es {
		fmt.Print(e.Output)
	}
	if !errors.Is(err, io.EOF) {
		log.Println(err)
		os.Exit(1)
	}
}
